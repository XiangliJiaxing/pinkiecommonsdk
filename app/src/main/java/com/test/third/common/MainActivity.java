package com.test.third.common;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Toast;

import com.jzy.pinkie.opensdk.PinkieAPI;
import com.jzy.pinkie.opensdk.PinkieAPIFactory;
import com.jzy.pinkie.opensdk.SendAuth;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        findViewById(R.id.tv_request).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                test(MainActivity.this);
            }
        });
    }

    private static void test(Context context) {
//        IWXAPI iwxapi = WXAPIFactory.createWXAPI(context, MyApplication.WECHAT_APP_ID, false);
//        if (!iwxapi.isPinkieAppInstalled()) {
//            ToastUtil.toastShort("您未安装微信客户端!");
//        } else {
//            SendAuth.Req req = new SendAuth.Req();
//            req.scope = "snsapi_userinfo";
//            req.state = "wechat_sdk_demo_test";
//            return iwxapi.sendReq(req);
//        }
        String APP_ID = "8GJMHI3Y345IYNGH";
        PinkieAPI pinkieAPI = PinkieAPIFactory.createApi(context, APP_ID, true); // todo: 校验签名逻辑
        if (!pinkieAPI.isPinkieAppInstalled()) {
            Toast.makeText(context.getApplicationContext(), "Pinkie 未安装！", Toast.LENGTH_SHORT).show();
        } else {
            SendAuth.AuthReq authReq = new SendAuth.AuthReq();
            authReq.scope = "snsapi_userinfo";
            authReq.state = "Z";
            pinkieAPI.sendReq(authReq);
        }
    }


    /**
     * http://192.168.8.52:8080/rainbowchat/LoginService/RequestCode?appid=8GJMHI3Y345IYNGH&scope=snsapi_userinfo&state=Z
     *
     */
}
