package com.test.third.pinkie;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.widget.Toast;

import com.jzy.pinkie.opensdk.BaseEntry;
import com.jzy.pinkie.opensdk.IPinkieAPIEventHandler;
import com.jzy.pinkie.opensdk.PinkieAPI;
import com.jzy.pinkie.opensdk.PinkieAPIFactory;
import com.jzy.pinkie.opensdk.SendAuth;

/**
 * Pinkie SDK 回调接口
 */
public class PinkieEntryActivity extends AppCompatActivity implements IPinkieAPIEventHandler {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String APP_ID = "8GJMHI3Y345IYNGH";
        PinkieAPI pinkieAPI = PinkieAPIFactory.createApi(this, APP_ID, false);
        pinkieAPI.handleIntent(getIntent(), this);
    }

    @Override
    public void onReq(BaseEntry.BaseReq var1) {

    }

    @Override
    public void onResp(BaseEntry.BaseResp var1) {
        String errorMsg = var1.getErrStr();
        switch (var1.errCode) {
            case BaseEntry.BaseResp.ErrCode.ERR_OK:
                int commandType = var1.getType();
                if (commandType == 1 && SendAuth.AuthResp.class.isInstance(var1)) {
                    SendAuth.AuthResp authResp = (SendAuth.AuthResp) var1;
                    String code = authResp.getCode();
                    Toast.makeText(PinkieEntryActivity.this.getApplicationContext(), "code:" + code, Toast.LENGTH_SHORT).show();
                    finish();
                }
                break;
            case BaseEntry.BaseResp.ErrCode.ERR_COMM:
                Toast.makeText(PinkieEntryActivity.this.getApplicationContext(), errorMsg, Toast.LENGTH_SHORT).show();
                finish();
                break;
            case BaseEntry.BaseResp.ErrCode.ERR_SENT_FAILED:
                Toast.makeText(PinkieEntryActivity.this.getApplicationContext(), errorMsg, Toast.LENGTH_SHORT).show();
                finish();
                break;
            case BaseEntry.BaseResp.ErrCode.ERR_AUTH_DENIED:
                Toast.makeText(PinkieEntryActivity.this.getApplicationContext(), "授权被取消", Toast.LENGTH_SHORT).show();
                finish();
                break;
            case BaseEntry.BaseResp.ErrCode.ERR_USER_CANCEL:
                Toast.makeText(PinkieEntryActivity.this.getApplicationContext(), "您取消了授权操作", Toast.LENGTH_SHORT).show();
                finish();
                break;
            case BaseEntry.BaseResp.ErrCode.ERR_BAN:
                Toast.makeText(PinkieEntryActivity.this.getApplicationContext(), TextUtils.isEmpty(var1.getErrStr()) ? "授权操作过程中出现问题，请重试" : var1.getErrStr(),
                        Toast.LENGTH_SHORT).show();
                finish();
            case BaseEntry.BaseResp.ErrCode.ERR_UNSUPPORT:
                Toast.makeText(PinkieEntryActivity.this.getApplicationContext(), TextUtils.isEmpty(var1.getErrStr()) ? "不受支持的请求" : var1.getErrStr(),
                        Toast.LENGTH_SHORT).show();
                finish();
                break;
        }
    }
}
